
local function config_surfaces()
    -- We will add layers to each configured main surface.
    -- These are the surfaces configured by the player in game.
    -- You can also generate layers on other maih surfaces.
	for _, surface_name in ipairs(remote.call("SurfacesAPI", "get_main_surfaces")) do
        remote.call(
            -- Call the register_surface function
            "SurfacesAPI", "register_surface",
            -- Name of the surfaces. We include the main surface name to make sure they are all unique
            surface_name.."-my-surface-name-suffix",
            -- Name of the main surface
            surface_name,
            -- Selection of layers, we want all the layers below the main surface.
            -- You can select any range by using numbers, 0 represents the main surface.
            -- "bottom" and "top" are special values for minus infinity and plus infinity respectively
            {"bottom", -1},
            -- Name of the interface as mentioned by the surface generation mod.
            -- Some examples are:
            -- SurfacesOriginalUnderground for Surfaces Original underground generation
            -- SurfacesOriginalSky for Surfaces Original sky generation
            -- SurfacesCavesUnderground for Surfaces Caves
            "SurfacesOriginalUnderground"
        )

        -- You can register more surfaces here by using remote.call again.
    end
end

script.on_init(function()
    -- Called when creating a new save file
    config_surfaces()
end)

script.on_configuration_changed(function()
    -- Called when mods are updated
    config_surfaces()
end)

-- For even more customization, refer to the Surfaces documentation and/or Factorio lua api reference.

